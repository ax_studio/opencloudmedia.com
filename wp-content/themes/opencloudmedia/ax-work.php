<?php
/*
Template Name: Projects
*/

get_header();
?>
    <main class="projects">
        <section>
            <?php
            while ( have_posts() ) : the_post();
                the_content();
            endwhile; // End of the loop.
            ?>
        </section>
    </main>


<?php
get_footer();
