function moodBackground() {
    if ($('.theme_dark').is(':visible')) {
        $('body').addClass('dark_theme');
        $('.custom-logo-link').show();
        $('.logo-dark').hide();
    } else {
        $('body').addClass('light_theme');
        $('.custom-logo-link').hide();
        $('.logo-dark').show();

    }
}


function whenScrolled() {


    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {

            // $('.service-1').addClass('invisible');
            // $('.service-2').addClass('invisible');
            // $('.service-3').addClass('invisible');
            // $('.service-4').addClass('invisible');
            // $('.service-5').addClass('invisible');
            // $('.service-6').addClass('invisible');

        }
    });
}




function growingTextArea() {
    $('textarea').attr('rows', '1');

    $('textarea').each(function () {
        this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
    }).on('input', function () {
        this.style.height = 'auto';
        this.style.height = (this.scrollHeight) + 'px';
    });
}