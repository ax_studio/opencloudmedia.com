<?php
/*
Template Name: Service
*/

get_header();
?>

    <main class="service">
        <?php get_template_part('template-parts/socialmedia', 'none'); ?>

        <section>
            <?php
            while (have_posts()) :
                the_post(); ?>

                <div class="container-sticky">
                    <h1 class="title-main"><?php the_title(); ?></h1>
                </div>

                <?php the_content();

                // If comments are open or we have at least one comment, load up the comment template.
                if (comments_open() || get_comments_number()) :
                    comments_template();
                endif;

            endwhile; // End of the loop.
            ?>

        </section><!-- #main -->
    </main>

<?php
get_footer();
